class D4SWorkspace {

    #d4sboot = null;
    #workspaceURL = null;

    constructor(workspaceURL, d4sboot) {
        this.#workspaceURL = workspaceURL;
        if (d4sboot) {
            this.#d4sboot = d4sboot;
        } else {
            this.#d4sboot = document.querySelector("d4s-boot-2");
        }
    }

    /**
     * Calls with a secure fetch the workspace with provided data.
     * To upload data by using a <code>FormData</code> object you have to leave the <code>mime</code> attribute as <code>null</code>.
     * @param {*} method the method to use for the call, default to <code>GET</code>
     * @param {*} uri the uri to invoke, relative to <code>workspaceURL</code> provided in the object's constructor
     * @param {*} body the payload to send
     * @param {*} mime the mime type of the payload
     * @param {*} extraHeaders extra HTTP headers to send
     * @returns the reponse payload as a promise, JSON data promise if the returned data is "application/json", a String data promise if "text/*" or a blob data promise in other cases
     */
    callWorkspace(method, uri, body, mime, extraHeaders) {
        let req = { };
        if (method) {
            req.method = method;
        }
        if (body) {
            req.body = body;
        }
        if (extraHeaders) {
            req.headers = extraHeaders;
        }
        if (mime) {
            if (req.headers) {
                req.headers["Content-Type"] = mime;
            } else {
                req.headers = { "Content-Type" : mime };
            }
        }
        const url = this.#workspaceURL + (uri.startsWith('/') ? uri : '/' + uri);
        return this.#d4sboot.secureFetch(url, req)
            .then(resp => {
                if (resp.ok) {
                    const contentType = resp.headers.get("content-type");
                    if (contentType && contentType.indexOf("application/json") !== -1) {
                        return resp.json();
                    } else if (contentType && contentType.indexOf("text/") !== -1) {
                        return resp.text();
                    } else {
                        return resp.blob();
                    }
                } else {
                    throw "Cannot invoke workspace via secure fetch for URL: " + url;
                }
            }).catch(err => {
                console.error(err);
                throw err;
            });
    }

    getWorkspace($extraHeaders) {
        return this.callWorkspace("GET", "", null, null, $extraHeaders)
            .then(json => {
                return json.item.id;
            }).catch(err => {
                const msg = "Cannot get workspace root ID";
                console.error(msg);
                throw msg;
            });
    }

    checkOrCreateFolder(parentFolderId, name, description, extraHeaders) {
        const baseURI = "/items/" + parentFolderId;
        console.log("Checking existance of folder: " + name);
        let uri =  baseURI + "/items/" + name;
        return this.callWorkspace("GET", uri, null, null, extraHeaders)
            .then(json => {
                if (json.itemlist[0]) {
                    const id = json.itemlist[0].id;
                    console.log("'" + name + "' folder exists with and has id: " + id);
                    return id;
                } else {
                    console.info("'" + name + "' folder doesn't exist, creating it: " + name);
                    uri = baseURI + "/create/FOLDER";
                    const params = new URLSearchParams({ name : name, description : description, hidden : false });
                    return this.callWorkspace("POST", uri, params.toString(), "application/x-www-form-urlencoded", extraHeaders).then(id => {
                        console.log("New '" + name + "' folder successfully created with id: " + id);
                        return id
                    });
                }
            }); 
    };

    uploadFile(parentFolderId, name, description, data, contentType, extraHeaders) {
        const uri = "/items/" + parentFolderId + "/create/FILE";
        const request = new FormData();
        request.append("name", name);
        request.append("description", description);
        request.append(
            "file",
            new Blob([data], {
                type: contentType
            })
        );
        return this.callWorkspace("POST", uri, request, null, extraHeaders)
            .then(id => {
                console.info("File '" + name + "' successfully uploaded and its id is: " + id);
                return true;
            }).catch(err => {
                console.error("Cannot upload file '" + name + "'. Error: " + err);
                return false;
            });
    };
}