class CrisprscanImporter extends AbstractImporter {

    constructor() {
        super(new CrisprscanHelper())
    }

    render(){
        this.getRootDoc().innerHTML = `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <div class="card">
                <div class="card-header">
                    <h5>Import from <a href="https://www.crisprscan.org" target="_new">CRISPRscan</a></h5>
                </div>
                <div class="card-body">
                    <label for="basic-url" class="form-label">Import a CRISPRscan TSV exported file or from a D4Science Workspace file share long URL (e.g. https://data.d4science.net/shub/E_TnRRST...)</label>
                    <div class="input-group mb-3">
                        <div><input class="btn btn-outline-primary" type="file" accept=".tsv" id="crispscan_file"><input class="btn btn-outline-primary" type="url" placeholder="https://data.d4science.net/shub/E_TnRRST..." pattern="https://data.d4science.net/shub/.*" size="34" id="crisprscan_url"></div>
                        <select class="form-select w-auto" id="crispscan_file_genome">
                            <option value="">[Select Genome]</option>
                            <option value="danRer10">Danio rerio (danRer10)</option>
                            <option value="danRer11">Danio rerio (danRer11)</option>
                            <option value="hg19">Homo sapiens (hg19)</option>
                            <option value="hg38">Homo sapiens (hg38)</option>
                            <option value="mm10">Mus musculus (mm10)</option>
                            <option value="mm39">Mus musculus (mm39)</option>
                        </select>
                        <select class="form-select w-auto" id="crispscan_file_chromosome" disabled="true">
                        <select>
                        <select class="form-select w-auto" id="crispscan_file_pam"/>
                            <option value="">[Select PAM]</option>
                            <option value="NGG">NGG</option>
                            <option value="NAG">NAG</option>
                            <option value="NGA">NGA</option>
                            <option value="NRG">NRG (R = A or G)</option>
                        </select>
                        <button class="btn btn-outline-primary" type="button" id="crispscan_file_import_button">Import</button>
                    </div>
                </div>
                <div class="card-footer">
                    <a name="trigger" class="d-none" href="#"><a/>
                </div>
            </div>
        `

        this.getRootDoc().querySelector("#crispscan_file_genome").addEventListener("change", ev=> {
            const genome = ev.target.value;
            const chromosomeSelect = this.getRootDoc().querySelector("#crispscan_file_chromosome");
            chromosomeSelect.innerHTML = "";
            if (genome != '') {
                iGeneIndexer.fetchGenomeChromosomesFromUSCS(genome).then(chromosomes => {
                    chromosomeSelect.innerHTML = '<option value="">[Select Chromosome]</option>';
                    for (const [name, index] of Object.entries(chromosomes).sort()) {
                        if (name.length < 6) {
                            const newOption = document.createElement("option");
                            newOption.text = name;
                            newOption.value = name;
                            chromosomeSelect.add(newOption);
                        }
                    }
                    chromosomeSelect.disabled = false;
                }).catch(err => alert(err));
            } else {
                chromosomeSelect.disabled = true;
            }
        });
        this.getRootDoc().querySelector("#crispscan_file_import_button").addEventListener("click", ev=> {
            const tsvFile = this.getRootDoc().querySelector("#crispscan_file").files[0];
            const tsvURL = this.getRootDoc().querySelector("#crisprscan_url").value;
            if ((tsvFile == null || tsvFile == '') && (tsvURL == null || tsvURL == '')) {
                alert("Please, select/enter the CRISPRscan's TSV file/URL to import first");
                return;
            }
            const genome = this.getRootDoc().querySelector("#crispscan_file_genome").value
            if (genome == null || genome == '') {
                alert("Please, enter the Genome first");
                return;
            }
            const chromosome = this.getRootDoc().querySelector("#crispscan_file_chromosome").value
            if (chromosome == null || chromosome == '') {
                alert("Please, enter the chromosome first");
                return;
            }
            const pam = this.getRootDoc().querySelector("#crispscan_file_pam").value
            if (pam == null || pam == '') {
                alert("Please, enter the PAM first");
                return;
            }
            this.getHelper().setDefaultChromosome(chromosome);
            if (tsvFile != null && tsvFile != '') {
                this.importTSVExport(tsvFile, genome, pam);
            } else {
                this.importTSVURL(tsvURL, genome, pam);
            }
        })

        this.getRootDoc().querySelector("a[name=trigger]").addEventListener("click", ev=>{
            ev.preventDefault()
            ev.stopPropagation()
            if(this.getDatadisplay()){
                this.getDatadisplay().show(this)
            }
        })
    }

    enableData(){
        const a = this.getRootDoc().querySelector("a[name=trigger]")
        a.textContent = `
            Show ${this.getTableLines().length} matches from imported TSV data
        `
        a.classList.remove("d-none")
    }

    getHeader(){
        return `Imported from CRISPRscan TSV file`
    }

    getRenderer(filter, output){
        if (filter.mode !== 're') {
            if(output === "html") {
                return new CrisprScanHTMLDimerTableRenderer(this, filter);
            } else if(output === "tsv2") {
                return new CrisprScanTSVDimerTableRenderer(this, filter, "tsv2")
            } else {
                return super.getRenderer(this, filter)
            }
        } else {
            return super.getRenderer(filter, output)
        }
    }

}

window.customElements.define('igene-crisprscan-importer', CrisprscanImporter);