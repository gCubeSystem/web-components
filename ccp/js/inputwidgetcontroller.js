class CCPInputWidgetController extends HTMLElement {

  #data = null;

  constructor() {
    super()
  }

  connectedCallback() {
    this.#data = JSON.parse(base64DecodeUnicode(this.getAttribute("input")))

    if (this.isChecklist()) {

      const opts = this.#data.schema.enum.join(",")
      this.innerHTML += `<d4s-ccp-input-checklist readonly="${this.#data.schema.readOnly}" options="${opts}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-checklist>`
      this.querySelector("d4s-ccp-input-checklist").default = this.#data.schema.default

    } else if (this.isEnum()) {

      const opts = this.#data.schema.enum.join(",")
      this.innerHTML += `<d4s-ccp-input-enum readonly="${this.#data.schema.readOnly}" options="${opts}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-enum>`
      this.querySelector("d4s-ccp-input-enum").default = this.#data.schema.default

    } else if (this.isCode()) {
      
      this.innerHTML += `<d4s-ccp-input-textarea readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-textarea>`
      this.querySelector("d4s-ccp-input-textarea").default = this.#data.schema.default

    } else if (this.isDateTime()) {
      
      const t = this.#data.schema.format.toLowerCase() === "datetime" ?
        "datetime-local" : this.#data.schema.format.toLowerCase()
      this.innerHTML += `<d4s-ccp-input-simple type="${t}" readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-simple>`
      this.querySelector("d4s-ccp-input-simple").default = this.#data.schema.default

    } else if (this.isFile()) {
      
      this.innerHTML += `<d4s-ccp-input-file readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-file>`
      this.querySelector("d4s-ccp-input-file").default = this.#data.schema.default

    } else if (this.isRemoteFile()) {
      
      this.innerHTML += `<d4s-ccp-input-remotefile readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-remotefile>`
      this.querySelector("d4s-ccp-input-remotefile").default = this.#data.schema.default

    } else if (this.isGeo()) {
      
      this.innerHTML += `<d4s-ccp-input-geo readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-remotefile>`
      this.querySelector("d4s-ccp-input-geo").default = this.#data.schema.default

    } else if (this.isSecret()) {
      
      this.innerHTML += `<d4s-ccp-input-secret readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs }" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-secret>`
      this.querySelector("d4s-ccp-input-secret").default = this.#data.schema.default

    } else if (this.isBoolean()) {
      
      this.innerHTML += `<d4s-ccp-input-boolean readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-boolean>`
      this.querySelector("d4s-ccp-input-boolean").default = this.#data.schema.default

    } else if (this.isNumber()) {
      
      this.innerHTML += `<d4s-ccp-input-simple type="number" readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-simple>`
      this.querySelector("d4s-ccp-input-simple").default = this.#data.schema.default

    } else {
      
      this.innerHTML += `<d4s-ccp-input-simple readonly="${this.#data.schema.readOnly}" default="${this.#data.schema.default}" maxOccurs="${this.#data.maxOccurs}" minOccurs="${this.#data.minOccurs}" name="${this.#data.id}" description="${base64EncodeUnicode(this.#data.description)}" title="${this.#data.title}"></d4s-ccp-input-simple>`
      this.querySelector("d4s-ccp-input-simple").default = this.#data.schema.default

    }
  }

  set value(value) {
    this.firstElementChild.value = value
  }

  get value() {
    return this.firstElementChild.value.length === 1 ?
      this.firstElementChild.value[0] :
      this.firstElementChild.value
  }

  get name() {
    return this.firstElementChild.name
  }

  isChecklist() {
    return this.isEnum() && (this.#data.schema.format === "checklist")
  }

  isNumber() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (this.#data.schema.format.toLowerCase() === "number")
  }

  isBoolean() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (this.#data.schema.format.toLowerCase() === "boolean")
  }

  isEnum() {
    return (this.#data.schema.type === "string") && ("enum" in this.#data.schema)
  }

  isSecret() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (this.#data.schema.format.toLowerCase() === "secret")
  }

  isCode() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (this.#data.schema.format.toLowerCase() === "code")
  }

  isFile() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (this.#data.schema.format.toLowerCase() === "file")
  }

  isRemoteFile() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (this.#data.schema.format.toLowerCase() === "remotefile")
  }

  isGeo() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (this.#data.schema.format.toLowerCase() === "geo")
  }

  isDateTime() {
    return (this.#data.schema.type === "string") &&
      ("format" in this.#data.schema) &&
      (this.#data.schema.format != null) &&
      (["date", "time", "datetime"].indexOf(this.#data.schema.format.toLowerCase()) !== -1)
  }
}
window.customElements.define('d4s-ccp-input', CCPInputWidgetController);

class CCPBaseInputWidgetController extends HTMLElement {
  #rootdoc = null;
  #minOccurs = 1;
  #maxOccurs = 1;
  #description = ""
  #title = ""
  #name = ""
  #default = null
  #options = null
  #value = null
  #readonly = false

  //useful to avoid having a custom element for every basic input type
  #type = null

  #count = 1

  constructor() {
    super()
    this.#rootdoc = this//this.attachShadow({ mode: "open" });
    this.#name = this.getAttribute("name")
    this.#title = this.getAttribute("title")
    this.#description = base64DecodeUnicode(this.getAttribute("description"))
    //this.#default = this.getAttribute("default")
    this.#minOccurs = Number(this.getAttribute("minoccurs") ? this.getAttribute("minoccurs") : 1)
    this.#maxOccurs = Number(this.getAttribute("maxoccurs") ? this.getAttribute("maxoccurs") : 1)
    this.#maxOccurs = Math.max(this.#minOccurs, this.#maxOccurs)
    this.#readonly = this.getAttribute("readonly") === "true"

    // coalesce all basic input types
    this.#type = this.getAttribute("type")

    // Handle enum case
    this.#options = (this.getAttribute("options") ? this.getAttribute("options").split(",") : null)

    //this.value = Array(Math.max(this.#minOccurs, 1)).fill(this.#default)
  }

  set default(def){
    this.#default = def
    const defarr = Array.isArray(def) ? def : [def]
    const min = Math.max(this.#minOccurs, 1)
    var v = []
    for(let j=0; j < this.#maxOccurs; j++){
      if(j < defarr.length){
        v.push(defarr[j])
      }else if(j < min){
        v.push(defarr[defarr.length - 1])
      }
    }
    this.value = v
  }

  setValue(v) {
    this.#value = v
    this.render()
  }

  set value(v) {
    const actual = Array.isArray(v) ? v : [v]
    if (actual.length < this.#minOccurs || actual.length > this.#maxOccurs) {
      throw `Value with length ${v.length} does not respect bounds [${this.minOccurs},${this.maxOccurs}]`
    }
    this.#value = actual
    this.render()
  }

  get value() {
    return this.#value
  }

  get rootdoc() {
    return this.#rootdoc
  }

  get required() {
    return this.#minOccurs > 0
  }

  get readonly() {
    return this.#readonly
  }

  isIncrementable() {
    return this.#value.length < this.#maxOccurs
  }

  isDecrementable() {
    return this.#value.length > this.#minOccurs && this.#value.length > 1
  }

  get count() {
    return this.#count
  }

  get name() {
    return this.#name
  }

  get title() {
    return this.#title
  }

  get description() {
    return this.#description
  }

  get default() {
    return this.#default
  }

  get options() {
    return this.#options
  }

  get type() {
    return this.#type
  }

  renderPlusMinus() {
    this.rootdoc.querySelector("div[name=plusminus]").innerHTML = `
        ${this.isIncrementable() ? `<span name="plus" title="add one" class="badge text-success border-success btn">+</span>` : ``}
        ${this.isDecrementable() ? `<span name="minus" title="remove one" class="badge border border-danger  text-danger btn">-</span>` : ``}
    `
  }

  renderContent() {
    this.rootdoc.querySelector("div[name=content]").innerHTML = this.content()
  }

  render() {
    this.rootdoc.innerHTML = `
      <div name="root" class="my-3 ccp-input-widget form-field row">
        <label class="form-label">
          <div class="d-flex justify-content-between">
            <div>
              ${this.required ? `<span title="Required" class="p-1 text-danger">*</span>` : ``}
              ${this.title}
            </div>
            <div name="tools" class="d-flex" style="gap:2px">
              <div name="plusminus">
              </div>
            </div>
          </div>
          <div>
            <small class="text-muted">${this.#description}</small>
          </div>
          <div name="content">
          </div>
        </label>
     </div>
    `

    this.renderPlusMinus()
    this.renderContent()

    this.#rootdoc.querySelector("div[name=root]").addEventListener("click", ev => {
      const src = ev.target.getAttribute("name")
      if (src === "plus") {
        this.#value.push(this.#value[this.#value.length - 1])
        this.renderPlusMinus()
        this.renderContent()
        ev.preventDefault()
      } else if (src === "minus") {
        this.#value.pop()
        this.renderPlusMinus()
        this.renderContent()
        ev.preventDefault()
      }
    })
  }
}

class CCPSimpleInputWidgetController extends CCPBaseInputWidgetController {

  constructor() {
    super()
  }

  connectedCallback() {
    this.rootdoc.addEventListener("input", ev => {
      if (ev.target.getAttribute("name") === this.name) {
        const index = Number(ev.target.getAttribute("data-index"))
        this.value[index] = ev.target.value
      }
    })
  }

  content() {
    if (this.value.length <= 1) {
      return `<input data-index="0" value="${this.value.length ? this.value[0] : ''}" class="my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}/>`
    }
    var out =
      this.value.map((c, i) => {
        return `
          <input name="${this.name}" value="${c}" data-index="${i}" class="ccp-input my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}/>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-simple', CCPSimpleInputWidgetController);

class CCPEnumInputWidgetController extends CCPBaseInputWidgetController {

  constructor() {
    super()
  }

  connectedCallback() {
    this.rootdoc.addEventListener("change", ev => {
      if (ev.target.getAttribute("name") === this.name) {
        const index = Number(ev.target.getAttribute("data-index"))
        this.value[index] = ev.target.value
      }
    })
  }

  content() {
    if (this.value.length <= 1) {
      const opts = this.options ?
        this.options.map(o => `<option value="${o}" ${this.value.length && o === this.value[0] ? 'selected' : ''}>${o}</option>`).join("\n") :
        ''
      return `<select data-index="0" value="${this.value.length ? this.value[0] : ''}" class="my-2 form-control" placeholder="${this.title}" name="${this.name}" ${this.readonly ? 'disabled' : ''} ${this.required ? 'required' : ''}>${opts}</select>`
    }
    var out =
      this.value.map((v, i) => {
        const opts = this.options ?
          this.options.map(o => `<option value="${o}" ${o === v ? 'selected' : ''}>${o}</option>`).join("\n") :
          ''
        return `
          <select data-index="${i}" value="${v}" class="my-2 form-control" placeholder="${this.title}" name="${this.name}" ${this.readonly ? 'disabled' : ''} ${this.required ? 'required' : ''}>${opts}</select>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-enum', CCPEnumInputWidgetController);

class CCPChecklistInputWidgetController extends CCPBaseInputWidgetController {

  constructor() {
    super()
  }

  connectedCallback() {
    this.rootdoc.addEventListener("change", ev => {
      if (ev.target.getAttribute("name") === this.name) {
        const index = Number(ev.target.getAttribute("data-index"))

        const elems = Array.prototype.slice.call(ev.currentTarget.querySelectorAll(`input[name='${this.name}'][data-index='${index}']`))

        this.value[index] = elems.filter(e => e.checked).map(e => e.value).join(",")
      }
    })
  }

  buildOpts(index, selections) {
    return this.options ? this.options.map(o => `
        <div class="form-check form-switch form-check-inline">
          <label>${o}</label>
          <input data-index="${index}" class="form-check-input" type="checkbox" name="${this.name}" value="${o}" ${this.readonly ? 'readonly' : ''} ${selections.indexOf(o) > -1 ? 'checked' : ''}/></div>
      `).join("\n") :
      `
        <div class="form-check form-switch form-check-inline">
          <span class="muted text-danger">No options supplied</span>
        </div>
      `
  }

  content() {
    if (this.value.length === 1) {
      const opts = this.buildOpts(0, this.value.length ? this.value[0].split(",") : [])
      return `
        <div class="my-2">
          ${opts}
        </div>
      `
    }
    var out =
      this.value.map((c, i) => {
        const opts = this.buildOpts(i, c.split(","))
        return `
          <div class="my-2">
            ${opts}
          </div>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-checklist', CCPChecklistInputWidgetController);

class CCPBooleanInputWidgetController extends CCPBaseInputWidgetController {

  constructor() {
    super()
  }

  connectedCallback() {
    this.rootdoc.addEventListener("input", ev => {
      if (ev.target.getAttribute("name") === this.name) {
        const index = Number(ev.target.getAttribute("data-index"))
        this.value[index] = ev.target.checked ? "true" : "false"
      }
    })
  }

  content() {
    if (this.value.length <= 1) {
      return `
        <div class="my-2 form-check form-switch form-check-inline">
          <input data-index="0" value="${this.value.length ? this.value[0] : false}" class="my-2 form-check-input" type="checkbox" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.value[0] === "true" || this.value[0] === true ? 'checked' : ''} value="${this.value[0]}"/>
        </div>
      `
    }
    var out =
      this.value.map((c, i) => {
        return `
          <div class="my-2 form-check form-switch form-check-inline">
            <input data-index="${i}" value="${c}" class="my-2 form-check-input" type="checkbox" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${c === "true"  || c === true ? 'checked' : ''}/>
          </div>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-boolean', CCPBooleanInputWidgetController);

class CCPTextAreaWidgetController extends CCPBaseInputWidgetController {

  constructor() {
    super()
  }

  connectedCallback() {
    this.rootdoc.addEventListener("input", ev => {
      if (ev.target.getAttribute("name") === this.name) {
        const index = Number(ev.target.getAttribute("data-index"))
        this.value[index] = ev.target.value
      }
    })
  }

  content() {
    if (this.value.length <= 1) {
      return `<textarea data-index="0" class="my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" rows="5" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}>${this.value.length ? this.value[0] : ''}</textarea>`
    }
    var out =
      this.value.map((c, i) => {
        return `
          <textarea name="${this.name}" data-index="${i}" class="ccp-input my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" rows="5" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}>${c}</textarea>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-textarea', CCPTextAreaWidgetController);

class CCPFileInputWidgetController extends CCPBaseInputWidgetController {

  constructor() {
    super()
  }

  connectedCallback() {
    this.rootdoc.addEventListener("change", ev => {
      const tgt = ev.target
      if (tgt.getAttribute("name") === this.name) {
        const index = Number(tgt.getAttribute("data-index"))
        const file = ev.target.files[0]
        if (file.size > 100 * 1024) {
          alert("This input allows only small files (100K). Use references instead ")
          ev.stopPropagation()
          ev.preventDefault()
          tgt.value = null
          return false
        }
        const reader = new FileReader()
        reader.addEventListener('load', ev => {
          let encoded = ev.target.result.toString().replace(/^data:(.*,)?/, '');
          if ((encoded.length % 4) > 0) {
            encoded += '='.repeat(4 - (encoded.length % 4));
          }
          this.value[index] = encoded
          this.querySelector("small[name=preview]").textContent = 
            encoded.length > 15 ? encoded.substring(0, 5) + "..." + encoded.substring(encoded.length - 5) : encoded
          const newev = new Event("input", { bubbles : true})
          this.dispatchEvent(newev)
        })
        reader.readAsDataURL(file)
      }
    })
  }

  content() {
    if (this.value.length <= 1) {
      const v = this.value.length ? this.value[0] : ''
      const preview = typeof(v) === "string" ? v.substring(0, 5) + "..." + v.substring(v.length - 5) : ""
      return `
        <input type="file" data-index="0" class="my-2 form-control" placeholder="${this.title}" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required && !v ? 'required' : ''} value="${v}"/>
        <small name ="preview" class="form-text text-muted">${preview}</small>
      `
    }
    var out =
      this.value.map((c, i) => {
        const preview = typeof(v) === "string" ? c.substring(0, 5) + "..." + c.substring(v.length - 5) : ""
        return `
          <input type="file" data-index="${i}" class="my-2 form-control" placeholder="${this.title}" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''} value="${c}"/>
          <small name ="preview" class="form-text text-muted">${preview}</small>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-file', CCPFileInputWidgetController);

class CCPRemoteFileInputWidgetController extends CCPBaseInputWidgetController {

  #publicorprotected_dialog = null;
  #target = null
  #publiclink = null;
  #protectedlink = null;
  #index = null;

  constructor() {
    super()
  }

  async setPublicLink(l){
    const b = document.querySelector("d4s-boot-2")
    const link = await b.secureFetch(l)
    if(link.ok){
      const result = await link.json()
      this.#target.value = this.value[this.#index] = result
      this.#publicorprotected_dialog.style.display = "none"
      this.#publicorprotected_dialog.classList.remove("show")
      this.#publiclink = this.#protectedlink = this.#target =  this.#index = null
      const newev = new Event("input", { bubbles : true})
      this.dispatchEvent(newev)
    }else{
      alert("Unable to get public link for item")
      this.#target.value = this.value[this.#index] = ""
    }
  }

  addToolContent() {
    const iss = document.querySelector("d4s-boot-2").loginToken.iss;
    const addresses = {
      "https://accounts.dev.d4science.org/auth/realms/d4science": "https://workspace-repository.dev.d4science.org/storagehub/workspace",
      "https://accounts.pre.d4science.org/auth/realms/d4science": "https://pre.d4science.org/workspace",
      "https://accounts.d4science.org/auth/realms/d4science": "https://api.d4science.org/workspace"
    };
    this.rootdoc.querySelector("div[name=tools]").innerHTML += `
      <svg name="trigger" style="width:24;height:24;fill:#007bff; cursor:pointer" viewBox="0 -960 960 960">
          <path d="M160-160q-33 0-56.5-23.5T80-240v-480q0-33 23.5-56.5T160-800h240l80 80h320q33 0 56.5 23.5T880-640H447l-80-80H160v480l96-320h684L837-217q-8 26-29.5 41.5T760-160H160Zm84-80h516l72-240H316l-72 240Zm0 0 72-240-72 240Zm-84-400v-80 80Z"/>
      </svg>
      <div name="ws" class="d-none position-absolute shadow border border-primary bg-light p-2" style="right:0;z-index:1000; line-height:1.5rem;overflow:hidden;padding:5px;">
        <div class="mb-1" style="border-bottom: solid 1px gray;">
          <div class="d-flex justify-content-between m-0">
            <h5 class="text-secondary m-0">Access your workspace</h5>
            <span class="material-icons btn text-danger p-0" style="font-weight:bold" name="closebtn">close</span>
          </div>
          <small class="text-muted m-0">Select an item or drag and drop it to a proper input</small>
        </div>
        <div style="min-width:500px; max-width:640px;overflow:auto;text-wrap:nowrap;text-overflow: ellipsis;min-height:5rem; max-height:10rem;">
          <d4s-storage-tree 
            base-url="${addresses[iss]}"
            file-download-enabled="true" 
            show-files="true"
            allow-drag="true"/>
        </div>
        <div class="d-flex justify-content-end mt-1 pt-1" style="border-top: solid 1px gray;">
          <span class="btn btn-primary" name="selectbtn">SELECT</span>
        </div>
      </div>
      <div class="modal fade" style="background-color:rgba(0,0,0,0.3)" name="publicorprotected" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content shadow-lg border-primary">
            <div class="modal-body">
              Choose whether you want to use the public link or the protected link (requires authentication and authorization).
            </div>
            <div class="modal-footer">
              <span name="public" class="btn btn-info">Public link</span>
              <span name="protected" class="btn btn-primary">Protected link</span>
              <span name="cancel" class="btn btn-danger">Cancel</span>
            </div>
          </div>
        </div>
      </div>
    `
    this.#publicorprotected_dialog = this.rootdoc.querySelector("div.modal[name=publicorprotected]")
    this.#publicorprotected_dialog.addEventListener("click", ev => {
      ev.stopPropagation()
      ev.preventDefault()
      const name = ev.target.getAttribute("name")
      if(this.#index != null && this.#target != null){
        if(name === "public") {
          this.setPublicLink(this.#publiclink);
        }
        else if(name === "protected"){
          this.#target.value = this.value[this.#index] = this.#protectedlink;
          this.#publicorprotected_dialog.style.display = "none"
          this.#publicorprotected_dialog.classList.remove("show")
          this.#publiclink = this.#protectedlink = this.#target =  this.#index = null
          const newev = new Event("input", { bubbles : true})
          this.dispatchEvent(newev)
        }else{
          this.#publicorprotected_dialog.style.display = "none"
          this.#publicorprotected_dialog.classList.remove("show")
          this.#publiclink = this.#protectedlink = this.#target =  this.#index = null
        }
      }
    })

    const ws = this.rootdoc.querySelector("div[name=ws]")
    const st = ws.querySelector("d4s-storage-tree")
    this.addEventListener("click", ev=>{
      ev.stopPropagation()
      ev.preventDefault()
      if (ev.target.getAttribute("name") == "selectbtn") {
        this.#publicorprotected_dialog.style.display = "block"
        this.#publicorprotected_dialog.classList.add("show")
        this.#target = this.querySelector("input")
        this.#index = 0
        this.#publiclink = st.d4sWorkspace.getPublicLink(st.currentId)
        this.#protectedlink = st.d4sWorkspace.getDownloadLink(st.currentId)
      }
    })
    this.rootdoc.querySelector("svg[name=trigger]").addEventListener("click", ev => {
      ws.classList.toggle("d-none")
      ev.preventDefault()
      ev.stopPropagation()
    })

    this.rootdoc.querySelector("span[name=closebtn]").addEventListener("click", ev => {
      ws.classList.add("d-none")
      ev.preventDefault()
      ev.stopPropagation()
    })

    this.addEventListener("dragover", ev => {
      ev.preventDefault()
    })

    this.addEventListener("drop", ev => {
      ev.stopPropagation()
      if (ev.target.getAttribute("name") == this.name && ev.target.getAttribute("data-index") != null) {
        this.#publicorprotected_dialog.style.display = "block"
        this.#publicorprotected_dialog.classList.add("show")
        this.#target = ev.target
        this.#index = Number(ev.target.getAttribute("data-index"))
        this.#publiclink = ev.dataTransfer.getData("text/plain+publiclink")
        this.#protectedlink = ev.dataTransfer.getData("text/plain+downloadlink")
      }
    })

    this.addEventListener("input", ev => {
      if (ev.target.getAttribute("name") == this.name && ev.target.getAttribute("data-index") != null) {
        this.value[Number(ev.target.getAttribute("data-index"))] = ev.target.value
      }
    })

    document.addEventListener("keydown", ev => {
      if (ev.code == 'Escape') ws.classList.add("d-none");
    })
  }

  content() {
    if(!this.readonly && !this.querySelector("div[name=ws]")) this.addToolContent();
    if (this.value.length <= 1) {
      return `
       <input data-index="0" value="${this.value.length ? this.value[0] : ''}" class="my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}/>`
    }
    var out =
      this.value.map((c, i) => {
        return `
          <input name="${this.name}" value="${c}" data-index="${i}" class="ccp-input my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}/>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-remotefile', CCPRemoteFileInputWidgetController);

class CCPSecretInputWidgetController extends CCPBaseInputWidgetController {

  constructor() {
    super()
  }

  connectedCallback() {
    this.rootdoc.addEventListener("input", ev => {
      if (ev.target.getAttribute("name") === this.name) {
        const index = Number(ev.target.getAttribute("data-index"))
        this.value[index] = ev.target.value
      }
    })

    this.rootdoc.addEventListener("click", ev => {
      if (ev.target.getAttribute("name") === "password_toggle") {
        const index = Number(ev.target.getAttribute("data-index"))
        const field = this.rootdoc.querySelector(`input[data-index='${index}']`)
        if (field.type === "text") field.type = "password";
        else field.type = "text";
      }
    })
  }

  content() {
    if (this.value.length <= 1) {
      return `
        <div style="position:relative">
          <input data-index="0" value="${this.value.length ? this.value[0] : ''}" class="my-2 form-control" placeholder="${this.title}" type="password" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}/>
          <span data-index="0" class="btn" style="position:absolute; right:0; bottom:0" name="password_toggle">&#128065;</span>
        </div>
        `
    }
    var out =
      this.value.map((c, i) => {
        return `
          <div style="position:relative">
            <input name="${this.name}" value="${c}" data-index="${i}" class="ccp-input my-2 form-control" placeholder="${this.title}" type="password" name="${this.name}" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}/>
            <span data-index="${i}" class="btn" style="position:absolute; right:0; bottom:0" name="password_toggle">&#128065;</span>
          </div>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-secret', CCPSecretInputWidgetController);

class CCPGeoInputWidgetController extends CCPBaseInputWidgetController {

  #format_dialog = null;
  #target = null
  #draw = null;
  #raster = null
  #source = null
  #map = null;
  #vector = null
  #index = null;
  #widget = null

  constructor() {
    super()
  }

  
  addToolContent() {
    
    this.rootdoc.querySelector("div[name=tools]").innerHTML += `
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  
      <svg name="trigger" style="width:24;height:24;fill:#007bff; cursor:pointer" viewBox="0 -960 960 960">
        <path d="M480-80q-83 0-156-31.5T197-197q-54-54-85.5-127T80-480q0-83 31.5-156T197-763q54-54 127-85.5T480-880q83 0 156 31.5T763-763q54 54 85.5 127T880-480q0 83-31.5 156T763-197q-54 54-127 85.5T480-80Zm-40-82v-78q-33 0-56.5-23.5T360-320v-40L168-552q-3 18-5.5 36t-2.5 36q0 121 79.5 212T440-162Zm276-102q20-22 36-47.5t26.5-53q10.5-27.5 16-56.5t5.5-59q0-98-54.5-179T600-776v16q0 33-23.5 56.5T520-680h-80v80q0 17-11.5 28.5T400-560h-80v80h240q17 0 28.5 11.5T600-440v120h40q26 0 47 15.5t29 40.5Z"/>
      </svg>
      <div name="map" class="d-none position-absolute shadow border border-primary bg-light p-2" style="right:0;z-index:1000;width:100%;line-height:1.5rem;overflow:hidden;padding:5px;">
        <div name="maptoolbar" class="mb-1" style="border-bottom: solid 1px gray;">
          <div class="d-flex justify-content-between m-0">
            <div>
              <span title="Draw a rectangular box" name="Box" class="btn text-primary p-0 material-icons">crop_16_9</span>
              <span title="Draw a circle" name="Circle" class="btn text-primary p-0 material-icons">radio_button_unchecked</span>
              <span title="Select a Point" name="Point" class="btn text-primary p-0 material-icons">my_location</span>
              <span title="Draw a Polygon" name="Polygon" class="btn text-primary p-0 material-icons" style="color:transparent">pentagon</span>
              <span title="Delete all" name="deletebtn" class="btn text-danger p-0 material-icons">delete</span>
            </div>
            <div>
              <span title="Set geometry" class="btn btn-primary" style="font-weight:bold" name="selectbtn">Export</span>
              <span title="Close" class="material-icons btn text-danger p-0" style="font-weight:bold" name="closebtn">close</span>
            </div>
          </div>
          <small class="text-muted m-0">Draw and export geometry</small>
        </div>
        <div style="width:100%;height:500px;position:relative;">
          <style>
            div[name=internalmap]{
              width: 100%;
              height:100%;
              position: relative;
            }
          </style>
          <div name="internalmap"></div>
        </div>
      </div>
      <div class="modal fade" style="background-color:rgba(0,0,0,0.3)" name="format" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content shadow-lg border-primary">
            <div class="modal-body">
              Choose export format.
            </div>
            <div class="modal-footer">
              <span title="Export WKT" name="wkt" class="btn btn-info">WKT</span>
              <span title="Export to GML" name="gml" class="btn btn-primary">GML</span>
              <span title="Export to GeoJSON" name="geojson" class="btn btn-secondary">GeoJSON</span>
              <span title="Cancel" name="cancel" class="btn btn-danger">Cancel</span>
            </div>
          </div>
        </div>
      </div>
    `
    
    // Init map
    this.init()

    this.rootdoc.querySelector("div[name=maptoolbar]").addEventListener("click", ev=>{
      const type = ev.target.getAttribute("name")
      if(type === "closebtn"){
        this.#widget.classList.add("d-none")
        ev.preventDefault()
        ev.stopPropagation()
      }if(type === "selectbtn"){
        this.#widget.classList.add("d-none")
        this.#format_dialog.style.display = "block"
        this.#format_dialog.classList.add("show")
        this.#index = this.value.length - 1
        const alltargets = this.#target = this.rootdoc.querySelectorAll("textarea")
        for(let i=0; i < alltargets.length;i++){
          if(!alltargets.item(i).value[i]){
            this.#index = i
            break;
          }
        }
        this.#target = this.rootdoc.querySelectorAll("textarea").item(this.#index)
        ev.preventDefault()
        ev.stopPropagation()
      }else if(["Circle", "Point", "Polygon", "Box"].indexOf(type) !== -1){
        ev.stopPropagation();
        ev.preventDefault();
        this.undoAllDrawings()
        if(this.#draw != null && this.#draw.type === type) return;
        if(this.#draw != null) this.#map.removeInteraction(this.#draw);
        this.#draw = new ol.interaction.Draw({
          source: this.#source, 
          type : type === "Box" ? "Circle" : type, 
          geometryFunction : type === "Box" ? ol.interaction.Draw.createBox() : null
        })
        this.#map.addInteraction(this.#draw)
      }else if(type === "deletebtn"){
        this.undoAllDrawings()
        ev.preventDefault()
        ev.stopPropagation()
      }
    })

    document.addEventListener("keydown", ev=>{
      if (ev.ctrlKey && ev.key === 'z') {
        this.undoDrawing()
        }else if(ev.key === "Escape"){
          this.stopDrawing()
        }
    })
    
    this.#format_dialog = this.rootdoc.querySelector("div.modal[name=format]")
    this.#format_dialog.addEventListener("click", ev => {
      ev.stopPropagation()
      ev.preventDefault()
      const features = this.#source.getFeatures()
      if( features.length && this.#index != null && this.#target != null){
        const name = ev.target.getAttribute("name")
        var format
        if(name === "cancel"){
          this.#format_dialog.style.display = "none"
          this.#format_dialog.classList.remove("show")
          return
        }
        if(name === "wkt") {
          format = new ol.format.WKT()
        }else if(name === "geojson"){
          format = new ol.format.GeoJSON()
        }else if(name === "gml"){
          format = new ol.format.GML()
        }
        const result = features.map(f=>{
          const geom = f.getGeometry().getType() === "Circle" ? ol.geom.Polygon.fromCircle(f.getGeometry(), 50) : f.getGeometry()
          const tgeom = geom.transform(this.#map.getView().getProjection(), new ol.proj.Projection({code: "EPSG:4326"}))
          return format.writeGeometry(tgeom)
        }).join("\n")
        
        this.#target.value = this.value[this.#index] = result
        const newev = new Event("input", { bubbles : true})
        this.dispatchEvent(newev)
      }
      
      this.#format_dialog.style.display = "none"
      this.#format_dialog.classList.remove("show")
    })

    this.rootdoc.querySelector("svg[name=trigger]").addEventListener("click", ev => {
      this.#widget.classList.toggle("d-none")
      ev.preventDefault()
      ev.stopPropagation()
    })

    this.rootdoc.addEventListener("input", ev=>{
      if(ev.target.getAttribute("name") === this.name && ev.target.getAttribute("data-index")){
        const index = Number(ev.target.getAttribute("data-index"))
        this.value[index] = ev.target.value
      }
    })
  }


  init(){
    this.#widget = this.rootdoc.querySelector("div[name=map]")
    this.#draw = null
    this.#raster = new ol.layer.Tile({source: new ol.source.OSM()});
		this.#source = new ol.source.Vector({wrapX: false});
		this.#vector = new ol.layer.Vector({source: this.#source});
    
    this.#map = new ol.Map({
      layers: [this.#raster, this.#vector],
      target: this.rootdoc.querySelector("div[name=internalmap]"),
      view: new ol.View({center: [0, 0], zoom: 4 }),
      controls :[]
    })
  }

  undoAllDrawings(){
    this.#source.clear()
  }

  undoDrawing(){
    const features = this.#source.getFeatures()
    if(features.length){
      this.#source.removeFeature(features[features.length-1])
    }
  }
  
  stopDrawing(){
    if(this.#draw){
      this.#draw.abortDrawing()
      this.#map.removeInteraction(this.#draw);
      this.#draw = null
    }
  }

  content() {
    if(!this.readonly && !this.querySelector("div[name=map]")) this.addToolContent();
    if (this.value.length <= 1) {
      return `
        <textarea name="${this.name}" data-index="0" class="ccp-input my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" rows="5" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}>${this.value.length ? this.value[0] : ''}</textarea>
      `
    }
    var out =
      this.value.map((c, i) => {
        return `
          <textarea name="${this.name}" data-index="${i}" class="ccp-input my-2 form-control" placeholder="${this.title}" type="${this.type}" name="${this.name}" rows="5" ${this.readonly ? 'readonly' : ''} ${this.required ? 'required' : ''}>${c}</textarea>
        `
      }).join("\n")
    return out
  }
}
window.customElements.define('d4s-ccp-input-geo', CCPGeoInputWidgetController);
