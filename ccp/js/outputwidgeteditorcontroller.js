class CCPOutputWidgetEditorController extends HTMLElement {
  
  #output = null
  #index = null
  #type = null
  #messages = {
    "en" : {
      "output_id_help" : "The Id of the output. This has to be unique accross all the outputs the method",
      "output_delete_help" : "Delete this output",
      "output_title_help" : "Title of the output. This is how the output will appear in forms.",
      "output_description_help" : "A description for this output",
      "min_occurs_help" : "Minimum cardinality",
      "max_occurs_help" : "Msximum cardinality",
      "mimetype_help" : "Set MIME type of expected output",
      "file_path" : "Path to file",
      "file_path_help" : "Define the path to the file holding the output. This documents the location of the output in the zip archive returned by the execution. Paths in the execution runtime instead are infrastructure dependant."
    }
  }
  #delete_icon = `
    <svg style="width:24px;height:24px;pointer-events: none;" viewBox="0 0 24 24">
      <path d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
    </svg>
  `
  
  getLabel(key, localehint){
    const locale = localehint ? localehint : navigator.language
    const actlocale = this.#messages[locale] ? locale : "en"
    const msg = this.#messages[actlocale][key]
    return msg == null || msg == undefined ? key : this.#messages[actlocale][key]
  }

  constructor(){
     super()
  }
  
  connectedCallback(controller){
    
  }
  
  get index(){
    return this.#index
  }
  
  render(output, i){
    this.#index = i
    this.#output = output
    this.#type = output.schema.enum ? "enum" : "string"
    this.innerHTML = `
        <details>
          <summary class="mb-3">
            <input class="form-control" style="width:auto;display:inline" required="required" name="id" value="${this.#output.id}" title="${this.getLabel('output_id_help')}"/>
            <button data-index="${this.#index}" name="delete-output" title="${this.getLabel('output_delete_help')}" class="btn btn-danger ccp-toolbar-button">
              ${this.#delete_icon} 
            </button>  
          </summary>
          <div style="padding-left: 1rem;border-left: 1px solid gray;">
            <div class="row mb-3">
              <div class="col">
                <div class="form-field" title="${this.getLabel('output_title_help')}">
                  <input name="title" class="form-control" placeholder="title" value="${this.#output.title}" required="required"/>
                </div>
              </div>
            </div>
            <div class="mb-3">
              <div class="form-field" title="${this.getLabel('output_description_help')}">
                <textarea rows="1" name="description" class="form-control" placeholder="description" required="required">${this.#output.description}</textarea>
              </div>
            </div>
            <div class="row mb-3 d-none">
              <div class="col">
                <div class="form-field" title="${this.getLabel("min_occurs_help")}">
                  <input value="${this.#output.minOccurs}" type="number" min="0" step="1" name="minOccurs" class="form-control" placeholder="minOccurs"/>
                </div>
              </div>
              <div class="col">
                <div class="form-field" title="${this.getLabel("max_occurs_help")}">
                  <input value="${this.#output.maxOccurs}" type="number" min="0" step="1" name="maxOccurs" class="form-control" placeholder="maxOccurs"/>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col d-none">
                <div class="form-field" title="Type">
                  <select name="type" class="form-control" placeholder="type">
                    <option value="string">String</option>
                  </select>
                </div>
              </div>
              <div class="col-3">
                <div class="form-field" title="${this.getLabel("mimetype_help")}">
                  <input value="${this.#output.schema.contentMediaType}" name="contentMediaType" class="form-control" placeholder="mime"/>
                </div>
              </div>
              ${this.renderMetadata()}
            </div>
          </div>
        </details>
    `
  
    this.addEventListener("input", ev=>{
      const val = ev.target.value
      const ename = ev.target.getAttribute("name")
      if(ename === "id"){
        this.#output.id = val
      }
      else if(ename === "title"){
        this.#output.title = val
      }
      else if(ename === "description"){
        this.#output.description = val
      }
      else if(ename === "minOccurs"){
        this.#output.minOccurs = val
      }
      else if(ename === "maxOccurs"){
        this.#output.maxOccurs = val
      }
      else if(ename === "contentMediaType"){
        this.#output.schema.contentMediaType = val
      }
      else if(ename === "href"){
        this.#output.metadata[0].href = val
        this.#output.metadata[0].title = val
      }
    })
  }
  
  renderMetadata(output){
    if(this.#output.metadata && this.#output.metadata.length > 0){
      return `
        <div class="col">
          <div class="form-field" title="${this.getLabel("file_path_help")}">
            
            <input value="${this.#output.metadata[0].href}" name="href" class="form-control" placeholder="${this.getLabel("file_path")}"/>
          </div>
        </div>    
      `
    }else return ""
    
  }
}

window.customElements.define('d4s-ccp-output-editor', CCPOutputWidgetEditorController);