function base64EncodeUnicode(str) {
    // Encode the string as UTF-8 bytes
    const utf8Bytes = new TextEncoder().encode(str);
    
    // Convert the bytes to a binary string
    let binary = '';
    utf8Bytes.forEach(byte => {
        binary += String.fromCharCode(byte);
    });
    
    // Encode the binary string to Base64
    return btoa(binary);
}

function base64DecodeUnicode(base64) {
    // Decode the Base64 string to a binary string
    const binary = atob(base64);
    
    // Convert the binary string to a Uint8Array
    const bytes = Uint8Array.from(binary, char => char.charCodeAt(0));
    
    // Decode the UTF-8 bytes back to a Unicode string
    const decoder = new TextDecoder();
    return decoder.decode(bytes);
}